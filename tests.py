#!/usr/bin/env/ python

import unittest
import yapkg
import os
from shutil import copytree, rmtree

PKGNAME = 'tmux'

GIT_URL = 'http://projects.archlinux.org'
GIT_DIRS = (
  '/svntogit/packages.git/plain/%s/trunk',
  '/svntogit/community.git/plain/%s/trunk'
)

class YapkgTests(unittest.TestCase):
    def setUp(self):
        print("Begin yapkg setup...")
        self.makepkg_path = '/bin/makepkg'
        self.editor_path = '/bin/vim'
        self.buildfolder = '/tmp/yabstmp'
        self.pkg = yapkg.Yapkg(self.makepkg_path, self.editor_path, self.buildfolder)
        self.pkgname = PKGNAME
        print("...end setup.\n")

    def test_get_buildfolder(self):
        """Verify that the buildfolder is:
        /tmp/yabstmp/pkgname."""
        print("Testing get_buildfolder()")
        buildfolder = os.path.join(self.buildfolder, self.pkgname)
        self.assertEqual(self.pkg.get_buildfolder(self.pkgname), buildfolder)
    
    def test_get_pkgbuild(self):
        """Verify that the pkgbuild is located in:
        /tmp/yabstmp/pkgname/PKGBUILD.
        """
        print("Testing get_pkgbuild()")
        pkgbuild = os.path.join(self.pkg.get_buildfolder(self.pkgname), 'PKGBUILD') 
        self.assertEqual(self.pkg.get_pkgbuild(self.pkgname), pkgbuild)
    
    def test_buildfolder_exists(self):
        """Verify that the function returns true when the folder 
        exists, and false when it does not."""
        print("Testing buildfolder_exists()")
        buildfolder = self.pkg.get_buildfolder(self.pkgname)
        print("Creating {}".format(buildfolder))
        os.makedirs(buildfolder, exist_ok=True)
        self.assertTrue(self.pkg.buildfolder_exists(self.pkgname))
        print("Removing {}".format(buildfolder))
        rmtree(buildfolder)
        self.assertFalse(self.pkg.buildfolder_exists(self.pkgname))

    def test_rm_buildfolder(self):
        """Verify that the function deletes the folder using 
        buildfolder_exists()."""
        print("Testing rm_buildfolder()")
        buildfolder = self.pkg.get_buildfolder(self.pkgname)
        print("Creating {}".format(buildfolder))
        os.makedirs(buildfolder, exist_ok=True)
        self.pkg.rm_buildfolder(self.pkgname)
        self.assertFalse(self.pkg.buildfolder_exists(self.pkgname))

    # these may be untestable
    #def test_make_pkg(self):
    #def test_open_editor(self):


class LocalPkgTests(unittest.TestCase):
    def setUp(self):
        print("Begin local setup...")
        self.makepkg_path = '/bin/makekpkg'
        self.editor_path = '/bin/vim'
        self.buildfolder = '/tmp/yabstmp/'
        self.absfolder = '/var/abs'
        self.pkg = yapkg.LocalPkg(self.makepkg_path, self.editor_path, self.buildfolder,
                          self.absfolder)
        self.pkgname = PKGNAME
        print("...end setup.\n")

    def test_check_pkg_exists(self):
        """Verify that the function returns true if the package exists and
        false if it does not."""
        pkgname = self.pkgname
        self.assertTrue(self.pkg.check_pkg_exists(pkgname))
        pkgname = 'asdf'
        self.assertFalse(self.pkg.check_pkg_exists(pkgname))

    def test_get_repo(self):
        """Verify that the function returns community."""
        print("Testing get_repo()")
        repo = "community"
        self.assertEqual(self.pkg.get_repo(self.pkgname), repo)
        repo = "invalid"
        self.assertNotEqual(self.pkg.get_repo(self.pkgname), repo)

    def test_get_pkgfolder(self):
        print("Testing get_pkgbuilder()")
        pkgfolder = os.path.join(self.absfolder, self.pkg.get_repo(self.pkgname), self.pkgname)
        self.assertEqual(self.pkg.get_pkgfolder(self.pkgname), pkgfolder)
        pkgfolder = "/var/aaa"
        self.assertNotEqual(self.pkg.get_pkgfolder(self.pkgname), pkgfolder)

    #def test_get_pkg(self):
    #def test_print_info(self):

class RemotePkgTests(unittest.TestCase):
    def setUp(self):
        print("Begin remote setup...")
        self.pkg = yapkg.RemotePkg('/bin/makepkg', '/bin/vim/', '/tmp/yabstmp',
                          GIT_DIRS, GIT_URL)
        self.pkgname = PKGNAME
        print("...end setup.\n")

    #def test_retrieve_file(self):

    #def test_get_git_links(self):

    #def test_get_pkg(self):

    def test_check_pkg_exists(self):
        """Verify that the function returns true if the package exists and
        false if it does not."""
        pkgname = self.pkgname
        self.assertTrue(self.pkg.check_pkg_exists(pkgname))
        pkgname = 'asdf'
        self.assertFalse(self.pkg.check_pkg_exists(pkgname))

    #def test_print_info(self):


if __name__ == '__main__':
    unittest.main()
