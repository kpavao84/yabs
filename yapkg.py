# This contains the classes and functions used for yabs

import os
import sys
import re
from urllib.error import HTTPError
from urllib.request import urlopen
from shutil import copytree, rmtree
from subprocess import call
from pathlib import Path


class Yapkg(object):
    """ Yet another package - this is the main class for yabs.

    Most of the functions in here will probably take the (pkgname) argument.
    This is because of a for loop in the main() function that goes through
    a list of packages.

    (arguments)
    makepkg_path:   path to makepkg (default: makepkg)
    editor_path:    path to the EDITOR (default: vim)
                    Used to open the PKGBUILD, not sure if this should remain
                    here or be moved out of the class
    buildfolder:    location of the directory used to build packages
    """

    def __init__(self, makepkg_path='makepkg', editor_path='vim',
                 buildfolder='/tmp/yabstmp'):
        self.makepkg_path = makepkg_path
        self.editor_path = editor_path
        self.buildfolder = buildfolder

    def get_buildfolder(self, pkgname):
        """Get the location that the package will be copied to/built in and
        return it, i.e. '/tmp/yabstmp/pacman'."""
        return os.path.join(self.buildfolder, pkgname)

    def get_pkgbuild(self, pkgname):
        """Get the location of the PKGBUILD path and return it. This is used
        with the open_editor() function."""
        pkgbuild = self.get_buildfolder(pkgname) + "/PKGBUILD"
        return pkgbuild

    def buildfolder_exists(self, pkgname):
        """Check to see if the build folder exists and return a boolean
        value. This is used to determine if we will ask to overwrite the
        folder."""
        if pkgname not in os.listdir(self.buildfolder):
            return False
        else:
            return True

    def rm_buildfolder(self, pkgname):
        """Ask whether or not to overwrite the packages build folder.
        This should only be called when buildfolder_exists() returns True."""
        try:
            print("Removing: {}".format(self.get_buildfolder(pkgname)))
            rmtree(self.get_buildfolder(pkgname))
        except PermissionError:
            print_error('Permission denied for: {}'.format(self.get_buildfolder(pkgname)))

    # TODO: allow passing of arguments to makepkg?
    def make_pkg(self, pkgname):
        """Go to the buildfolder and run makepkg. This must be run after
        get_pkg()."""
        print("Starting makepkg...")
        os.chdir(self.get_buildfolder(pkgname))
        call([self.makepkg_path, "-si"])

    def open_editor(self, pkgname):
        """Ask whether or not to edit the PKGBUILD using the default editor."""
        query = y_or_n("Do you want to edit the PKGBUILD?", default="no")
        if not query:
            return
        if not self.editor_path:
            print_warn("Please set the EDITOR environment variable in order to\
                    edit the PKGBUILD")
            return
        else:
            call([self.editor_path, self.get_pkgbuild(pkgname)])

# TODO: return all search results from pkgfolder and use yabs user input to choose 
class LocalPkg(Yapkg):
    """Class used for the local abs tree

    (extra arguments)
    absfolder:      this is usually /var/abs unless specified otherwise
    """

    def __init__(self, makepkg_path='makepkg', editor_path='vim',
                 buildfolder='/tmp/yabstmp', absfolder='/var/abs'):
        self.makepkg_path = makepkg_path
        self.editor_path = editor_path
        self.buildfolder = buildfolder
        self.absfolder = Path(absfolder)

    def check_pkg_exists(self, pkgname):
        """Make sure the package exists. Used because the RemotePkg class
        uses a different way of checking."""
        if self.get_pkgfolder(pkgname):
            return True
        else:
            return False

    def get_repo(self, pkgname):
        """Get the repository of the package."""
        pkgfolder = list(self.absfolder.glob('**/' + pkgname))
        return pkgfolder[0].parents[0].stem
            
    def get_pkgfolder(self, pkgname):
        """Convert the pkgfolder path to a string so its readable by get_pkg() 
        i.e. '/var/abs/core/pacman'."""
        pkgfolder = list(self.absfolder.glob('**/' + pkgname))
        if pkgfolder and pkgfolder[0].is_dir():
            return str(pkgfolder[0])

    def get_pkg(self, pkgname):
        """Copy the folder from the location in the abs folder to the build
        folder."""
        try:
            print("Copying: {} to: {}".format(self.get_pkgfolder(pkgname), self.buildfolder))
            copytree(self.get_pkgfolder(pkgname), self.get_buildfolder(pkgname))
        except PermissionError:
             print_error('Permission denied.')

    def print_info(self, pkgname):
        """Print out some useful information about the package."""
        print("repo:            {}".format(self.get_repo(pkgname)))
        print("abs location:    {}".format(self.get_pkgfolder(pkgname)))
        print("build folder:    {}".format(self.get_buildfolder(pkgname)))


class RemotePkg(Yapkg):
    """Class used for retrieving packages from the git repo.
    
    (extra arguments): git_dirs, git_url
    """
 
    def __init__(self, makepkg_path='makepkg', editor_path='vim',
                  buildfolder='/tmp/yabstmp', git_dirs=None, git_url=None):
        self.makepkg_path = makepkg_path
        self.editor_path = editor_path
        self.buildfolder = buildfolder
        self.git_url = git_url
        self.git_dirs = git_dirs

    # Original Author: Xyne
    # From: pbget
    def retrieve_file(self, url, path):
        """Download file at url to path."""
        with urlopen(url) as f:
            with open(path, 'wb') as g:
                buf = f.read(0x1000)
                while buf:
                    g.write(buf)
                    buf = f.read(0x1000)

    # Original Author: Xyne
    # From: pbget
    def get_git_links(self, pkgname):
        """Search the Git interface on archlinux.org."""
        for git_dir in self.git_dirs:
            url = self.git_url + git_dir % pkgname
            try:
                with urlopen(url) as f:
                    for line in f:
                        m = re.search(r"href='(.+?)'>(.+?)<".encode('utf-8'),
                                      line)
                        if m:
                            href = m.group(1).decode()
                            name = m.group(2).decode()
                            if name[:2] != '..':
                                yield self.git_url + href, name
                    return
            except HTTPError as error:
                if error.code != 404:
                    print_error('HTTP error: %s %s (%s)' % (error.code,
                                     error.msg, error.url))

    # Original Author: Xyne
    # From: pbget
    def get_pkg(self, pkgname):
        """Retrieve the files and copy them to the build folder."""
        urls = list(self.get_git_links(pkgname))
        pkgdir = os.path.join(self.buildfolder, pkgname)
        os.makedirs(pkgdir, exist_ok=True)
        for href, name in urls:
            print("retrieving {}".format(name))
            self.retrieve_file(href, os.path.join(pkgdir,
                                                 name))

    def check_pkg_exists(self, pkgname):
        """Make sure the package exists. Used because the LocalPkg class
        uses a different way of checking."""
        if not (list(self.get_git_links(pkgname))):
            return False
        else:
            return True

    def print_info(self, pkgname):
        """Print out some useful information about the package."""
        urls = list(self.get_git_links(pkgname))
        # print("repo:            " + self.get_repo(pkgname))
        print("url:             {}".format(os.path.dirname(urls[0][0])))
        print("build folder:    {}".format(self.get_buildfolder(pkgname)))


# FUNCTIONS

# Original Author: "Thiago Kenji Okada"
#   "Modified version from http://stackoverflow.com/a/3041990, rewrite
#    some parts to be more Pythonic."
# From: 'burgaur'
# slightly modified by me
def y_or_n(question, default=None):
    """Asks the user to answer (y)es or (n)no to the question passed to
    the function.

    (arguments)
    question:   the yes or no question being asked
    default:    the default answer if <ENTER> is pressed. This should be
    passed as a "yes" or "no". If no value passed, the question will be
    asked until'y' or 'n' is entered
    """
    valid = {"yes": True, "y": True,
             "no": False, "n": False}
    if default is None:
        prompt = "[y/n] "
    elif default == "yes":
        prompt = "[Y/n] "
    elif default == "no":
        prompt = "[y/N] "
    else:
        raise ValueError("Invalid entry")

    while True:
        print(question, prompt, end="")
        choice = input().lower()
        try:
            if choice:
                return valid[choice]
            else:
                return valid[default]
        except KeyError:
            print_warn("Please respond with (y)es or (n)o.")

def print_warn(text, **kwargs):
    """Print out a warning."""
    print(text, file=sys.stderr, **kwargs)

def print_error(text, **kwargs):
    """Print out an error and exit the program."""
    print(text, file=sys.stderr, **kwargs)
    sys.exit(1)
