       ██         ██                      ██             
      ██  ██████ ░░██   ██   ██          ░██             
     ██  ░██░░░██ ░░██ ░░██ ██   ██████  ░██       ██████
    ░██  ░██  ░██  ░██  ░░███   ░░░░░░██ ░██████  ██░░░░ 
    ░██  ░██████   ░██   ░██     ███████ ░██░░░██░░█████ 
    ░░██ ░██░░░    ██    ██     ██░░░░██ ░██  ░██ ░░░░░██
     ░░██░██      ██    ██     ░░████████░██████  ██████ 
      ░░ ░░      ░░    ░░       ░░░░░░░░ ░░░░░   ░░░░░░  

     ▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
    ░▓                     ▓
    ░▓ python + abs = yabs ▓
    ░▓                     ▓
    ░▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓
    ░░░░░░░░░░░░░░░░░░░░░░░

   
# (probably) yet another build system

version: 0.0.4

status:  experimental

`yabs` is a python utility that allows you to automate the building of packages from the Arch Build System -- `abs`. It also allows retreiving PKGBUILDS from the official git repository. 

This is under **_HEAVY_** development!! Everything can and probably will change. Sorry if it breaks anything.

# Usage

    usage: yabs [-h] [--bdir <path>] [--remote] [--local] [--absdir <path>] [--nobuild] [--quiet]
                [<pkgname> [<pkgname> ...]]

    positional arguments:
      <pkgname>        The source package to retrieve.

    optional arguments:
      -h, --help       show this help message and exit
      --bdir <path>     Set the output directory. (default: /tmp/yabstmp)
      --remote         Retrieve from the official git repositories
      --local          Retrieve from youre local ABS tree (DEFAULT)
      --absdir <path>  Set the abs directory. (default: /var/abs)
      --nobuild        Only copy the package to the directory. Do not build it.
      --quiet, -q       Do not ask to edit the PKGBUILD.

You must first run `abs` to populate the `/var/abs/` directory for `--local` to work!

And if you haven't gotten `abs` yet, you should probably do that too:

    pacman -S abs

#### A simple example would be:

    yabs herbstluftwm

This copies the `/var/abs/community/herbstluftwm/` folder to `/tmp/yabstmp/herbstluftwm/` and then builds it.

#### A slightly more complicated example would be:

    yabs --bdir ~/pkgs/ --nobuild herbstluftwm

This copies the `/var/abs/community/herbstluftwm/` folder to `~/pkgs/herbstluftwm/` but does not build it.

# WHY?

Because the ABS should have a way of building packages similar to the AUR tools available. It is written in python because it is easy to read and hack. I am also doing this because I tried to build something similar in bash a few years ago for my netbook, but did not get very far. This is my attempt at something that will actually work. Maybe some other people will find it useful.

# TODO

#### Version 0.1 will include:
* [x] fix all major bugs
* [X] get the files from the official git repos
* [X] allow editing of `PKGBUILD` before running `makepkg`
* [ ] a `PKGBUILD` for you to use on the AUR

#### Version 1.0 (and all versions in between) may include:
- use other repos besides core, extra, and community
- handle dependencies
- handle more `makepkg` arguments
- use a config file
- beautify the output
- better `--help` and `man`
- add some wrappers to use `pacman`
- more as I think of them...

# BUGS 
- Hopefully none

# CONTRIBUTIONS
Contributions, bug reports, and feature requests welcome.

Keep in mind this is a personal project I am doing: for fun, to help me learn some programming concepts, and because why not. 

# THANKS
- [m45t3r](https://github.com/m45t3r) for `burgaur` which now shares some code with and provides inspiration for `yabs` 
- [Xyne](http://xyne.archlinux.ca/) for `pbget` which now shares some code with `yabs`
- [Xero](https://github.com/xero) for `3d.flf` which was used to make the ascii logo

