#!/usr/bin/env python
import os
from distutils.core import setup
from imp import load_source

yabs = load_source("yabs", "yabs")

def read(fname):
    filename = os.path.join(os.path.dirname(__file__), fname)
    return open(filename).read().replace('#', '')

setup(
    name='yabs',
    version=yabs.__version__,
    author=yabs.__author__,
    author_email=yabs.__email__,
    description=('(Probably) Yet Another Build System'),
    url='https://gitlab.com/kpavao84/yabs',
    py_modules=['yapkg'],
    scripts=['yabs'],
)
